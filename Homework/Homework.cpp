﻿#include <iostream>
#include <string>
using namespace std;

class Animal {
private:
	string Name = "DefautName";
public:
	virtual void voice() {
		cout << "*Default animal sound*\n";
	}
};

class Cat : public Animal {
public:
	void voice() {
		cout << "Meow\n";
	}
};

class Dog : public Animal {
public:
	void voice() {
		cout << "Bark\n";
	}
};

class Programmer : public Animal {
public:
	void voice() {
		cout << "Hello, world!\n";
	}
};

int main() {
	Animal** animals = new Animal*[4];
	animals[0] = new Animal();
	animals[1] = new Cat();
	animals[2] = new Dog();
	animals[3] = new Programmer();

	for (int i = 0; i < 4; i++)
		animals[i]->voice();

	for (int i = 0; i < 4; i++)
		delete animals[i];
	delete[] animals;
}